# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/drfaust92/bitbucket" {
  version     = "2.4.1"
  constraints = "~> 2.0"
  hashes = [
    "h1:ZOw2kA61PATDa3riWGraRHmaJ3pjMLLFAgda1hzHYCA=",
    "zh:1cfe1b6bb506c55b41cb053d974109811c91868f2a1b6a9fa1f07e647b5d08d8",
    "zh:33ce7149edf74dfc42378f70315077b608328d381d9f24c631c49d796fe353a0",
    "zh:38e0ccb10a23f7207a6eb7d96b7fce490107860b4b768028933ebd5dc21d663b",
    "zh:44932f3ebdbd568ca5861ac71aa31d4a32b04218db4cc85441cb1fcecb3acc16",
    "zh:69ee199115f59a04b7812f91d269e5c96c439701cf37a9a80da953b431107a56",
    "zh:6f02c7b9e02ad22ac898d0e63ad711e7b416cb505182c0de697ccbba6b501439",
    "zh:7a16a2776c4e501d7d4f1cf1a188e15b1886d362d2f2ecee9b4194821345bd4e",
    "zh:858b02ddd3c361ae9a3611a6edffae96d5537257b4807e4b663e70958d9d9f86",
    "zh:93b0205b20ed3265067b5f64834ef0dd5726ed8bf20777b5dfe4b827a278cefb",
    "zh:a1dc695ef102077287342403bb674ba5f583203113fdda7f64ed7b79988a2914",
    "zh:b3eb8ec9a777bd528bb5f4dbf982fe21adb212d61f394e709d91cd58f1c46e01",
    "zh:ba4a3a12d300b1b2ab3e3a966a6b154de3da0c28496ad55293c18e0a1eac7f19",
    "zh:faa187bc3842958253dd809570eb4ff77b053b51d7fccf6d55367ae467afa7f7",
  ]
}
